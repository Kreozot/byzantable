jQuery.fn.myAddClass = function (classTitle) {
	return this.each(function() {
		var oldClass = jQuery(this).attr("class");
		oldClass = oldClass ? oldClass : '';
		var startpos = oldClass.indexOf(classTitle);
		if (startpos >= 0)
			return;
		jQuery(this).attr("class", (oldClass+" "+classTitle).trim());
	});
};

jQuery.fn.myRemoveClass = function (classTitle) {
	return this.each(function() {
		var oldClass = jQuery(this).attr("class");
		var startpos = oldClass.indexOf(classTitle);
		if (startpos < 0)
			return;
		var endpos = startpos + classTitle.length;
		var newClass = oldClass.substring(0, startpos).trim() + " " + oldClass.substring(endpos).trim();
		if (!newClass.trim())
			jQuery(this).removeAttr("class");
		else
			jQuery(this).attr("class", newClass.trim());
	});
};

jQuery.fn.myHasClass = function (classTitle) {
	return jQuery(this).first().attr("class").indexOf(classTitle) >= 0;
};

jQuery(window).load(function () {
	var svgobject = document.getElementById('noteTable');
	if ('contentDocument' in svgobject) {
		var svgdom = jQuery(svgobject.contentDocument);

		var sounds = new Array();

		initSound = function(num, freq) {
			sounds[num] = T("osc", {wave:"sin", freq:freq})
			sounds[num].on("pause", function () {
				jQuery("g.note-g" + num, svgdom).myRemoveClass("note-g-active");
				sounds[num].bang();
			})		
		};

		initSound(1, 587);
		initSound(3, 565);
		initSound(7, 523);
		initSound(11, 484);
		initSound(13, 467);
		initSound(14, 457);
		initSound(16, 440);
		initSound(18, 423);
		initSound(22, 392);
		initSound(24, 378);
		initSound(28, 349);
		initSound(30, 336);
		initSound(31, 330);
		initSound(32, 323);
		initSound(34, 311);
		initSound(37, 294);
		initSound(39, 283);
		initSound(43, 262);
		initSound(47, 242);

		activateNote = function(col, num) {
			if (!sounds[num].isPlaying)
				sounds[num].play();
			jQuery("#column" + col + " g.note-g" + num, svgdom).myAddClass("note-g-active");
		}

		var selectorColumn = [];
		for (var i = 1; i <= 7; i++)
			selectorColumn[i] = jQuery("#column" + i, svgdom);

		onKeyDown = function(event) {
			if (selectorColumn[1].myHasClass("hover")) {
				switch (event.which) {
					case 48: activateNote(1, 1); break
					case 57: activateNote(1, 7); break
					case 56: activateNote(1, 11); break
					case 55: activateNote(1, 13); break
					case 54: activateNote(1, 16); break
					case 53: activateNote(1, 22); break
					case 52: activateNote(1, 28); break
					case 51: activateNote(1, 32); break
					case 50: activateNote(1, 37); break
					case 49: activateNote(1, 43); break
					case 192: activateNote(1, 47); break
				}
			} else if (selectorColumn[2].myHasClass("hover")) {
				switch (event.which) {
					case 57: activateNote(2, 1); break
					case 56: activateNote(2, 7); break
					case 55: activateNote(2, 11); break
					case 54: activateNote(2, 18); break
					case 53: activateNote(2, 22); break
					case 52: activateNote(2, 28); break
					case 51: activateNote(2, 32); break
					case 50: activateNote(2, 39); break
					case 49: activateNote(2, 43); break
				}
			} else if (selectorColumn[3].myHasClass("hover")) {
				switch (event.which) {
					case 57: activateNote(3, 1); break
					case 56: activateNote(3, 3); break
					case 55: activateNote(3, 13); break
					case 54: activateNote(3, 16); break
					case 53: activateNote(3, 22); break
					case 52: activateNote(3, 24); break
					case 51: activateNote(3, 34); break
					case 50: activateNote(3, 37); break
					case 49: activateNote(3, 43); break
				}
			} else if (selectorColumn[4].myHasClass("hover")) {
				switch (event.which) {
					case 53: activateNote(4, 22); break
					case 52: activateNote(4, 24); break
					case 51: activateNote(4, 32); break
					case 50: activateNote(4, 34); break
					case 49: activateNote(4, 43); break
				}
			} else if (selectorColumn[5].myHasClass("hover")) {
				switch (event.which) {
					case 53: activateNote(5, 22); break
					case 52: activateNote(5, 24); break
					case 51: activateNote(5, 30); break
					case 50: activateNote(5, 37); break
					case 49: activateNote(5, 43); break
				}
			} else if (selectorColumn[6].myHasClass("hover")) {
				switch (event.which) {
					case 53: activateNote(6, 7); break
					case 52: activateNote(6, 14); break
					case 51: activateNote(6, 16); break
					case 50: activateNote(6, 18); break
					case 49: activateNote(6, 28); break
				}
			} else if (selectorColumn[7].myHasClass("hover")) {
				switch (event.which) {
					case 57: activateNote(7, 1); break
					case 56: activateNote(7, 7); break
					case 55: activateNote(7, 13); break
					case 54: activateNote(7, 16); break
					case 53: activateNote(7, 22); break
					case 52: activateNote(7, 28); break
					case 51: activateNote(7, 31); break
					case 50: activateNote(7, 37); break
					case 49: activateNote(7, 43); break
				}
			}
			jQuery("svg", svgdom).off("keydown");
			jQuery(document).off("keydown");
		}

		onKeyUp = function(event) {
			for (var i in sounds) {
				if (!sounds.hasOwnProperty(i)) 
					continue;				
				sounds[i].pause();
			}
			jQuery("svg", svgdom).keydown(onKeyDown);
			jQuery(document).keydown(onKeyDown);
		}

		for (var i in sounds) {
			if (!sounds.hasOwnProperty(i)) 
				continue;
			jQuery("g.note-g" + i, svgdom).mousedown((function(i) {
					return function() {sounds[i].play();}
				})(i))
				.mouseup((function(i) {
					return function() {sounds[i].pause();}
				})(i))
				.mouseleave((function(i) {
					return function() {sounds[i].pause();}
				})(i));
		}

		jQuery(".column", svgdom).mouseenter(function() {
				jQuery(this).myAddClass("hover");
			})
			.mouseleave(function() {
				jQuery(this).myRemoveClass("hover");
			});

		jQuery("svg", svgdom).keydown(onKeyDown);
		jQuery(document).keydown(onKeyDown);

		jQuery("svg", svgdom).keyup(onKeyUp);
		jQuery(document).keyup(onKeyUp);
	}	
});