<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/2000/svg">

  <xsl:variable name="xOffset" select="100"/>
  <xsl:variable name="yOffset" select="40"/>
  <xsl:variable name="yStep" select="18"/>
  <xsl:variable name="columnWidth" select="100"/>
  <xsl:variable name="columnSpace" select="80"/>

<!--   <xsl:output
    method="xml"
    indent="yes"
    standalone="no"
    doctype-public="-//W3C//DTD SVG 1.1//EN"
    doctype-system="http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"
    media-type="image/svg" /> -->


    <!-- Деление шкалы -->
    <xsl:template match="scale-line">
      <line class="scale-orig" x1="{$xOffset - 15}" y1="{$yOffset + ((@num - 1) * $yStep)}" x2="{$xOffset - 5}" y2="{$yOffset + ((@num - 1) * $yStep)}"/>
    </xsl:template>

    <!--  Названия звука на шкале -->
    <xsl:template match="scale-text">
      <text class="text-sound-name" x="{$xOffset - 20}" y="{$yOffset + ((@num - 1) * $yStep) + 28}"><xsl:value-of select="."/></text>
    </xsl:template>


    <xsl:template match="tspan">
      <xsl:param name="xmod">
        <xsl:choose>
          <xsl:when test="@xmod != 0"><xsl:value-of select="@xmod"/></xsl:when>
          <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <xsl:param name="ymod">
        <xsl:choose>
          <xsl:when test="@ymod != 0"><xsl:value-of select="@ymod"/></xsl:when>
          <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <xsl:param name="size">
        <xsl:choose>
          <xsl:when test="@size != 0"><xsl:value-of select="@size"/></xsl:when>
          <xsl:otherwise>18</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <xsl:variable name="dy">
        <xsl:choose>
          <xsl:when test="position()=1">0</xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="@dy != 0"><xsl:value-of select="@dy"/></xsl:when>
              <xsl:otherwise>10pt</xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <tspan x="{$xOffset + (../../@num - 1) * ($columnWidth + $columnSpace) + $columnWidth + 4 + $xmod}" dy="{$dy}" style="font-family: {@font}; font-size: {$size};"><xsl:value-of select="."/></tspan>
    </xsl:template>

    <!--  Названия тона в столбце -->
    <xsl:template match="note-text">
      <xsl:param name="mod">
        <xsl:choose>
          <xsl:when test="@mod != 0"><xsl:value-of select="@mod"/></xsl:when>
          <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <text class="text-note-background" x="{$xOffset + (../@num - 1) * ($columnWidth + $columnSpace) + $columnWidth + 3}" y="{$yOffset + ((@num - 1) * $yStep) - 4 + $mod * $yStep}">
        <xsl:choose>
          <xsl:when test="tspan">
            <xsl:apply-templates select="tspan" />
          </xsl:when>
          <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
        </xsl:choose>
      </text>
      <text class="text-note" x="{$xOffset + (../@num - 1) * ($columnWidth + $columnSpace) + $columnWidth + 3}" y="{$yOffset + ((@num - 1) * $yStep) - 4 + $mod * $yStep}">
        <xsl:choose>
          <xsl:when test="tspan">
            <xsl:apply-templates select="tspan" />
          </xsl:when>
          <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
        </xsl:choose>
      </text>
    </xsl:template>

    <!-- Текст -->
    <xsl:template match="text">
      <xsl:param name="mod">
        <xsl:choose>
          <xsl:when test="@mod != 1"><xsl:value-of select="@mod"/></xsl:when>
          <xsl:otherwise>1</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <text class="text" x="{$xOffset + (../@num - 1) * ($columnWidth + $columnSpace) + $columnWidth * $mod * 0.5}" y="{$yOffset + (@startNum + (@endNum - @startNum) div 2 - 1) * $yStep + 6}">
        <xsl:choose>
          <xsl:when test="tspan">
            <xsl:for-each select='tspan'>
              <xsl:variable name="dy">
                <xsl:choose>
                  <xsl:when test="position()=1">0</xsl:when>
                  <xsl:otherwise>18pt</xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <tspan x="{$xOffset + (../../@num - 1) * ($columnWidth + $columnSpace) + $columnWidth * $mod * 0.5}" dy="{$dy}"><xsl:value-of select="."/></tspan>
            </xsl:for-each>
          </xsl:when>
          <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
        </xsl:choose>
      </text>
    </xsl:template>

    <!-- Цифра с размером интервала -->
    <xsl:template match="interval-text">
      <xsl:param name="mod">
        <xsl:choose>
          <xsl:when test="@mod != 1"><xsl:value-of select="@mod"/></xsl:when>
          <xsl:otherwise>1</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <text class="text-interval" x="{$xOffset + (../@num - 1) * ($columnWidth + $columnSpace) + $columnWidth * $mod * 0.5}" y="{$yOffset + (@startNum + (@endNum - @startNum) div 2 - 1) * $yStep + 6}"><xsl:value-of select="(@endNum - @startNum) * 2"/></text>
    </xsl:template>

    <!-- Фон столбцов -->
    <xsl:template match="background">      
      <rect class="column-background" x="{$xOffset + (../@num - 1) * ($columnWidth + $columnSpace)}" y="{$yOffset + (@startNum - 1) * $yStep}" height="{(@endNum - @startNum) * $yStep}" width="{$columnWidth}"/>
    </xsl:template>

    <!-- Нотная линия -->
    <xsl:template match="note">
      <xsl:param name="mod">
        <xsl:choose>
          <xsl:when test="@mod != 1"><xsl:value-of select="@mod"/></xsl:when>
          <xsl:otherwise>1</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <xsl:param name="offsetMod">
        <xsl:choose>
          <xsl:when test="@offsetMod != 0"><xsl:value-of select="@offsetMod"/></xsl:when>
          <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <g class="note-g note-g{@num}">
        <line class="scale" x1="{$xOffset - 17}" y1="{$yOffset + ((@num - 1) * $yStep)}" x2="{$xOffset - 3}" y2="{$yOffset + ((@num - 1) * $yStep)}"/>
        <line class="note" x1="{$xOffset + (../@num - 1) * ($columnWidth + $columnSpace) + $offsetMod * $columnWidth}" y1="{$yOffset + ((@num - 1) * $yStep)}" x2="{$xOffset + (../@num - 1) * ($columnWidth + $columnSpace) + $columnWidth * $mod + $offsetMod * $columnWidth}" y2="{$yOffset + ((@num - 1) * $yStep)}"/>
        <line class="note-back note{@num}" x1="{$xOffset + (../@num - 1) * ($columnWidth + $columnSpace) + $offsetMod * $columnWidth}" y1="{$yOffset + ((@num - 1) * $yStep)}" x2="{$xOffset + (../@num - 1) * ($columnWidth + $columnSpace) + $columnWidth * $mod + $offsetMod * $columnWidth}" y2="{$yOffset + ((@num - 1) * $yStep)}"/>
        <xsl:if test="@hotkey">
          <rect class="hotkey-background" x="{$xOffset + (../@num - 1) * ($columnWidth + $columnSpace) + $columnWidth * $mod * 0.5 + $offsetMod * $columnWidth - 9}" y="{$yOffset + ((@num - 1) * $yStep) - 9}" height="18" width="18" rx="4" ry="4"/>
          <text class="text-hotkey" x="{$xOffset + (../@num - 1) * ($columnWidth + $columnSpace) + $columnWidth * $mod * 0.5 + $offsetMod * $columnWidth}" y="{$yOffset + ((@num - 1) * $yStep + 5)}"><xsl:value-of select="@hotkey"/></text>
        </xsl:if>
      </g>
    </xsl:template>

    <!-- Вертикальная линия -->
    <xsl:template match="vertical-line">
      <xsl:param name="class">
        <xsl:choose>
          <xsl:when test="@dashed">dash-line</xsl:when>
          <xsl:otherwise>solid-line</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <line class="{$class}" x1="{$xOffset + (../@num - 1) * ($columnWidth + $columnSpace) + @mod * $columnWidth}" y1="{$yOffset + (@startNum - 1) * $yStep}" x2="{$xOffset + (../@num - 1) * ($columnWidth + $columnSpace) + @mod * $columnWidth}" y2="{$yOffset + (@endNum - 1) * $yStep}"/>
    </xsl:template>

    <!-- Линия между столбцами -->
    <xsl:template match="intercolumn-line">
      <xsl:param name="class">
        <xsl:choose>
          <xsl:when test="@dashed">dash-line</xsl:when>
          <xsl:otherwise>solid-line</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <line class="{$class}" x1="{$xOffset + (@startCol - 1) * ($columnWidth + $columnSpace) + $columnWidth + 2}" y1="{$yOffset + (@startNum - 1) * $yStep}" x2="{$xOffset + (@endCol - 1) * ($columnWidth + $columnSpace)}" y2="{$yOffset + (@endNum - 1) * $yStep}"/>
    </xsl:template>

    <!-- Фигурная скобка -->
    <xsl:template match="bracket">
      <path d="M{$xOffset + (../@num - 1) * ($columnWidth + $columnSpace) + $columnWidth},{$yOffset + (@startNum - 1) * $yStep} 
        a20,10 0 0,1 20,10
        l0,{((@endNum - @startNum) * $yStep - 40) div 2}
        a15,10 0 0,0 15,10
        a15,10 0 0,0 -15,10
        l0,{((@endNum - @startNum) * $yStep - 40) div 2}    
        a20,10 0 0,1 -20,10" class="figure"/>
    </xsl:template>

    <xsl:template match="columns">
      <xsl:for-each select="column">
        <g class="column" id="column{@num}">
          <xsl:apply-templates select="background" />
          <xsl:apply-templates select="interval-text" />
          <xsl:apply-templates select="note" />
          <xsl:apply-templates select="vertical-line" />
          <xsl:apply-templates select="text" />
          <xsl:apply-templates select="bracket" />
          <xsl:apply-templates select="note-text" />
        </g>
      </xsl:for-each>
    </xsl:template>

    <xsl:template match="table">
      <svg width="{$xOffset + 7 * ($columnWidth + $columnSpace)}" height="{$yOffset + 47 * $yStep + 50}" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
          <style type="text/css">
            @font-face {
              font-family: 'Psaltica';
              src: url('fonts/psaltica.eot');
              src: local('☺'), 
                url('fonts/psaltica.woff') format('woff'), 
                url('fonts/psaltica.ttf') format('truetype'), 
                url('fonts/psaltica.svg') format('svg');
              font-weight: normal;
              font-style: normal;
            }

            @font-face {
              font-family: 'Fthora';
              src: url('fonts/fthora.eot');
              src: local('☺'), 
                url('fonts/fthora.woff') format('woff'), 
                url('fonts/fthora.ttf') format('truetype'), 
                url('fonts/fthora.svg') format('svg');
              font-weight: normal;
              font-style: normal;
            }

            @font-face {
              font-family: 'Oxeia';
              src: url('fonts/oxeia.eot');
              src: local('☺'), 
                url('fonts/oxeia.woff') format('woff'), 
                url('fonts/oxeia.ttf') format('truetype'), 
                url('fonts/oxeia.svg') format('svg');
              font-weight: normal;
              font-style: normal;
            }

            .note-g {
              stroke-width: 2px;
              stroke: #000000;
              cursor: pointer;
            }

            .note-g:hover {
              stroke-width: 3px;
            }

            .note-g:active, .note-g-active {
              stroke: #ff0000;
              stroke-width: 3px;
            }

            .note {   
            }

            .note-back {
              stroke-opacity: 0;
              stroke-width: 10px;
            }

            .scale {
              stroke-opacity: 0;
              pointer-events: none;
            }
            .note-g:hover .scale {
              stroke-opacity: 1;
            }
            .note-g:active .scale, .note-g-active .scale {
              stroke-opacity: 1;
            }

            .scale-orig {
              stroke-width: 2px;
              stroke: #000000;
            }

            .dash-line {
              stroke-dasharray: 6,4;
              stroke-width: 1px;
              stroke: #333333;
            }

            .solid-line {
              stroke-width: 1px;
              stroke: #000000;
            }

            .column-background {
              fill: #ffffff;
              fill-opacity: 0.5;
            }
            .column:hover .column-background {
              fill: #f4f4f4;
              fill-opacity: 0.9;
            }

            .hotkey-background {
              fill: #ffffff;
              fill-opacity: 0;
              stroke-width: 1px;
              stroke: #555555;
              stroke-opacity: 0;
            }
            .column:hover .hotkey-background {
              fill-opacity: 1;
              stroke-opacity: 1;
            }
            .text-hotkey {
              text-anchor: middle;
              font-size: 14px;
              pointer-events: none;
              stroke: none;
              fill-opacity: 0;
            }
            .column:hover .text-hotkey {
              fill-opacity: 1;
            }

            .text {
              text-anchor: middle;
              font-size: 18px;
            }
            .text-interval {
              text-anchor: middle;
              font-size: 24px;
              pointer-events: none;
            }
            .text-sound-name {
              text-anchor: end;
              font-size: 18pt;
              font-family: Oxeia;
              pointer-events: none;
            }
            .text-note {
              text-anchor: start;
              font-size: 16px;
              pointer-events: none;
            }
            .text-note-background {
              text-anchor: start;
              font-size: 16px;
              pointer-events: none;
              stroke: #ffffff;
              stroke-width: 4px;
              fill: #ffffff;
              background: #00ff00;
            }

            .figure {
              stroke: #000000;
              fill: none;
              pointer-events: none;
            }
          </style>
        </defs>
        <g class="scales">
          <xsl:apply-templates select="scale-lines" />
        </g>
        <xsl:apply-templates select="intercolumn-line" />

        <xsl:apply-templates select="columns" />
      </svg>
    </xsl:template>
  </xsl:stylesheet>