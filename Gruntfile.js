module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		secret: grunt.file.readJSON('secret.json'),
		uglify: {
			build: {
				files: {
					'build/js/script.min.js' : [
						'js/script.js'
					]
				}
			}
		},
		xmlmin: {
			build: {
				files: [
	      			{expand: true, src: ['svg/*.xsl'], dest: 'build/'},
	      			{expand: true, src: ['svg/*.svg'], dest: 'build/'}
				]
			}
		},
		cssmin: {
			build: {
				files: {
					'build/css/main.css': [
						'css/main.css',
					]
				}
			}
		},
		processhtml: {
			build: {
				options: {
					data: {
						version: '<%= pkg.version %>'
					}
				},
				files: {
					'build/index.html': 'index.html'
				}
			}
		},
		htmlmin: {
			build: {
				options: {
					removeComments: true,
					collapseWhitespace: false,
					minifyJS: true
				},
				files: [
					{'build/index.html': 'build/index.html'},
					{'build/old_browser.html': 'old_browser.html'}
				]
			}
		},
		clean: {
			build: {
				src: [
					'build/*'
				]
			}
		},
		copy: {
			build: {
				files: [
					{
						src: [
							'js/thirdparty/*',
							'svg/fonts/*',
						],
						dest: 'build/'
					},
					{
						expand: true,
						flatten: true,
						src: ['favicons/**'],
						dest: 'build/',
						filter: 'isFile'
					}
				]
			}
		},
		'ftp-deploy': {
			production: {
				auth: {
					host: '<%= secret.ftp.host %>',
					port: '<%= secret.ftp.port %>',
					authPath: 'secret.json',
					authKey: 'ftpkey'
				},
				src: 'build/',
				dest: 'public_html/'
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-rename');
	grunt.loadNpmTasks('grunt-xmlmin');
	grunt.loadNpmTasks('grunt-processhtml');
	grunt.loadNpmTasks('grunt-ftp-deploy');

	grunt.registerTask('build', [
		'clean:build', 
		'uglify:build', 
		'cssmin:build',
		'xmlmin:build',
		'processhtml:build',
		'htmlmin:build',
		'copy:build'
	]);
	grunt.registerTask('default', ['build']);
	grunt.registerTask('deploy', ['ftp-deploy:production']);

};